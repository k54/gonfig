package gonfig

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"reflect"
	"unsafe"
)

// Sentinel errors that can be returned (wrapped), use errors.Is to check against them
var (
	ErrUnsettableField = errors.New("cannot set field")
	ErrNoDefault       = errors.New("no default tag present")
	ErrBadTarget       = errors.New("passed value is not a pointer to a struct")
	ErrInvalidName     = errors.New("name is invalid")
)

type Loader struct {
	NoFlags   bool   // if true, doesn't do the flag step
	NoEnvs    bool   // if true, doesn't do the environment variables step
	EnvPrefix string // prefix to add to all env variable names

	fieldsLeft map[string]fieldValue
	target     reflect.Value
}

func (l *Loader) stepFlags() error {
	flagSet := flag.NewFlagSet("conf", flag.ExitOnError)
	flagNamesToFieldName := map[string]string{}

	for fieldName, field := range l.fieldsLeft {
		flagName := field.field.Tag.Get("flag")
		if flagName == "-" {
			continue
		}
		flagName = cleanString(flagName)

		if flagName == "" {
			flagName = toFlagName(fieldName)
			// flagName = ConvertCase(fieldName, '-')
		}
		ok := isNameValid(flagName)
		if !ok {
			return fmt.Errorf("invalid flag name for field `%s` for `%s`: %w", flagName, field.field.Tag.Get("flag"), ErrInvalidName)
		}
		flagNamesToFieldName[flagName] = fieldName
		// fmt.Println(fieldName, field.field.Name, field.field.Tag.Get("default"))
		ptr := unsafe.Pointer((&field).value.UnsafeAddr()) //#nosec
		helpText := field.field.Tag.Get("help")

		switch field.field.Type.Kind() {
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			flagSet.Uint64Var((*uint64)(ptr), flagName, 0, helpText)
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			flagSet.Int64Var((*int64)(ptr), flagName, 0, helpText)
		case reflect.String:
			flagSet.StringVar((*string)(ptr), flagName, "", helpText)
		case reflect.Bool:
			flagSet.BoolVar((*bool)(ptr), flagName, false, helpText)
		default:
			if field.value.Type().Implements(reflect.TypeOf((*flag.Value)(nil)).Elem()) {
				flagger := field.value.Interface().(flag.Value)
				flagSet.Var(flagger, flagName, helpText)

			}
		}
	}
	err := flagSet.Parse(os.Args[1:])
	if err != nil {
		return err
	}
	flagSet.Visit(func(f *flag.Flag) {
		delete(l.fieldsLeft, flagNamesToFieldName[f.Name])
	})

	return nil
}
func (l *Loader) stepEnvs() error {
	// fields not set by flags
	for fieldName, field := range l.fieldsLeft {
		envName := field.field.Tag.Get("env")
		if envName == "-" {
			continue
		}
		envName = cleanString(envName)
		if envName == "" {
			envName = toEnvName(fieldName)
			// fmt.Println("env name", toEnvName(fieldName))
		}
		envName = l.EnvPrefix + envName
		ok := isNameValid(envName)
		if !ok {
			return fmt.Errorf("invalid env name for field `%s` for `%s`: %w", envName, field.field.Tag.Get("flag"), ErrInvalidName)
		}

		envValue, ok := os.LookupEnv(envName)
		if !ok {
			// should lookup file
			continue
		}

		delete(l.fieldsLeft, fieldName)
		fieldV := field.value
		if !fieldV.CanSet() {
			return fmt.Errorf("unsettable field `%v`: %w", fieldV, ErrUnsettableField)
		}
		err := fillReflectValue(fieldV, envValue)
		if err != nil {
			return err
		}
	}
	return nil
}
func (l *Loader) stepDefaults() error {
	for fieldName, field := range l.fieldsLeft {
		_ = fieldName

		defaultTag := field.field.Tag.Get("default")
		err := fillReflectValue(field.value, defaultTag)
		if err != nil {
			return err
		}
	}
	return nil
}

type fieldValue struct {
	field reflect.StructField
	value reflect.Value
}

func (l *Loader) recurse(start string, v reflect.Value) {
	if v.Kind() == reflect.Ptr {
		l.recurse(start, v.Elem())
		return
	}
	if v.Kind() != reflect.Struct {
		return
	}

	for i := 0; i < v.NumField(); i++ {
		field := v.Type().Field(i)
		name := field.Name
		if start != "" {
			name = start + "." + name
		}

		if field.Type.Kind() == reflect.Struct {
			l.recurse(name, v.Field(i))
			continue
		}
		l.fieldsLeft[name] = fieldValue{field: field, value: v.Field(i)}
	}
}
func (l *Loader) init(target interface{}) error {
	cv := reflect.ValueOf(target)
	if cv.Kind() != reflect.Ptr || cv.Elem().Kind() != reflect.Struct {
		return ErrBadTarget
	}

	l.fieldsLeft = make(map[string]fieldValue)
	l.target = cv.Elem()
	l.recurse("", l.target)

	return nil
}

func (l *Loader) validate() error {
	for fieldName, field := range l.fieldsLeft {
		_ = fieldName
		defaultTag, ok := field.field.Tag.Lookup("default")
		if !ok {
			return ErrNoDefault
		}
		zero := reflect.New(field.field.Type).Elem()
		err := fillReflectValue(zero, defaultTag)
		if err != nil {
			return err
		}
	}

	return nil
}

// Load parses your os.Args flags, your environment variables and your structs' `default` tags
// (in that order) then sets your structs' fields their the inferred value
func (l *Loader) Load(to interface{}) error {
	err := l.init(to)
	if err != nil {
		return err
	}

	err = l.validate()
	if err != nil {
		return err
	}

	if !l.NoFlags {
		err = l.stepFlags()
		if err != nil {
			return err
		}
	}
	if !l.NoEnvs {
		err = l.stepEnvs()
		if err != nil {
			return err
		}
	}
	err = l.stepDefaults()
	if err != nil {
		return err
	}

	return nil
}

func (l *Loader) GetEnvNames(to interface{}) (map[string]string, error) {
	err := l.init(to)
	if err != nil {
		return nil, err
	}

	err = l.validate()
	if err != nil {
		return nil, err
	}

	ret := make(map[string]string)

	for fieldName := range l.fieldsLeft {
		ret[fieldName] = toEnvName(fieldName)
	}

	return ret, nil
}

func (l *Loader) GetFlagNames(to interface{}) (map[string]string, error) {
	err := l.init(to)
	if err != nil {
		return nil, err
	}

	err = l.validate()
	if err != nil {
		return nil, err
	}

	ret := make(map[string]string)

	for fieldName := range l.fieldsLeft {
		ret[fieldName] = toFlagName(fieldName)
	}

	return ret, nil
}

// Load creates a zero-valued Loader and calls its Load function
func Load(to interface{}) error {
	return new(Loader).Load(to)
}
