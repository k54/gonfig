/*
package gonfig defines a simple and scalable way to get your service configuration from flags or env variables

To start, simply create a Config struct like so:

type Config struct {
	Port int `default:"8080" help:"port on which to listen"`
	Name string `default:"service" help:"name of the service"`
}

Then you simply:

conf := new(Config)
err := gonfig.Load(conf)

And then check the returned error for any problem that occured

The env variable name and flag name are nferred by the field name, but youcan override them by using the `env` and `flag` struct tags.

If the `env` or `flag` tags are set to "-", this field will not be set in the corresponding way.

*/

package gonfig
