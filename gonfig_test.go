package gonfig

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"reflect"
	"testing"
)

/*
defaults:
flag: log-level
env: LOG_LEVEL
file: log_level
*/
type boolean struct {
	B bool
}

func (b *boolean) String() string {
	if b == nil {
		return "???"
	}
	if b.B {
		return "true"
	}
	return "false"
}
func (b *boolean) Set(v string) error {
	if b == nil {
		return fmt.Errorf("nil receiver")
	}
	switch v {
	case "true":
		b.B = true
	case "false":
		b.B = false
	default:
		return fmt.Errorf("'%s' not a boolean value", v)
	}
	return nil
}

var _ flag.Value = (*boolean)(nil)

type Config struct {
	Port     uint16   `default:"3630" help:"port to bind to"`
	Number   int      `default:"42" help:"some number"`
	LogLevel string   `default:"info" help:"logging verbosity"`
	Testing  *boolean `default:"true" help:"boolean value lol"`
	Stuff    struct {
		Foo int `default:"12" help:"random number, is useless"`
		Bar struct {
			Thing int `default:"666"`
		}
	}
	// Testing  logLevel `default:"alert"`
}

// call like
// defer setArgs([]string{"/bin/mysoft", "-log-level", "alert"})()
func setArgs(args ...string) func() {
	oldArgs := os.Args
	os.Args = args
	return func() {
		os.Args = oldArgs
	}
}

// call like
// defer setEnv("PORT", "42000")()
func setEnv(key, value string) func() {
	oldv, ok := os.LookupEnv(key)
	os.Setenv(key, value)
	return func() {
		if ok {
			os.Setenv(key, oldv)
		} else {
			os.Unsetenv(key)
		}
	}
}

func TestToFlagName(t *testing.T) {
	var cases = []struct {
		In  string
		Out string
	}{
		{"HTMLValue", "html-value"},
		{"HtmlValue", "html-value"},
		{"Html_Value", "html-value"},
		{"Html_VALUE", "html-value"},
		{"HTML_value", "html-value"},
		{"HTML", "html"},
		{"IsItA", "is-it-a"},
		{"IsIt3", "is-it-3"},
		{"LogLevel", "log-level"},
		{"LogLevelB", "log-level-b"},
		{"S1a", "s-1a"},
		{"S1A", "s1a"},
		{"AaAaAaAa", "aa-aa-aa-aa"},
		{"AaAaAAAA", "aa-aa-aaaa"},
		{"SAa", "s-aa"},
		{"X漢_字", "x漢-字"},
	}

	for _, pass := range cases {
		out := toFlagName(pass.In)
		if out != pass.Out {
			t.Errorf("flagname(%q) is %q, should be %q", pass.In, out, pass.Out)
		}
	}
}

func TestToEnvName(t *testing.T) {
	var cases = []struct {
		In  string
		Out string
	}{
		{"HTMLValue", "HTML_VALUE"},
		{"HtmlValue", "HTML_VALUE"},
		{"Html_Value", "HTML_VALUE"},
		{"Html_VALUE", "HTML_VALUE"},
		{"HTML_value", "HTML_VALUE"},
		{"HTML", "HTML"},
		{"IsItA", "IS_IT_A"},
		{"IsIt3", "IS_IT_3"},
		{"LogLevel", "LOG_LEVEL"},
		{"LogLevelB", "LOG_LEVEL_B"},
		{"S1a", "S_1A"},
		{"S1A", "S1A"},
		{"AaAaAaAa", "AA_AA_AA_AA"},
		{"AaAaAAAA", "AA_AA_AAAA"},
		{"SAa", "S_AA"},
		{"X漢_字", "X漢_字"},
	}

	for _, pass := range cases {
		out := toEnvName(pass.In)
		if out != pass.Out {
			t.Errorf("envname(%q) is %q, should be %q", pass.In, out, pass.Out)
		}
	}
}

func TestMain(t *testing.T) {
	defer setArgs("/bin/mysoft", "-log-level", "alert")()
	defer setEnv("PORT", "4242")()
	defer setEnv("TESTING", "true")()
	defer setEnv("STUFF_FOO", "42")()

	var conf Config
	Load(&conf)
	ok := conf.Port == 4242 &&
		conf.Number == 42 &&
		conf.LogLevel == "alert" &&
		conf.Testing.B == true &&
		conf.Stuff.Foo == 42 &&
		conf.Stuff.Bar.Thing == 666

	if !ok {
		t.Fatal("values are not expected ones", conf)
	}
}

func TestBasic(t *testing.T) {
	defer setArgs("/bin/mysoft")()

	var conf Config
	Load(&conf)
	ok := conf.Port == 3630 &&
		conf.Number == 42 &&
		conf.LogLevel == "info" &&
		conf.Testing.B == true &&
		conf.Stuff.Foo == 12 &&
		conf.Stuff.Bar.Thing == 666

	if !ok {
		t.Fatal("values are not expected ones", conf)
	}
}

func TestEnvNames(t *testing.T) {
	defer setArgs("/bin/mysoft")()

	var conf Config
	vals, err := new(Loader).GetEnvNames(&conf)

	if err != nil {
		t.Fatal(err)
	}

	expected := map[string]string{
		"LogLevel":        "LOG_LEVEL",
		"Number":          "NUMBER",
		"Port":            "PORT",
		"Stuff.Bar.Thing": "STUFF_BAR_THING",
		"Stuff.Foo":       "STUFF_FOO",
		"Testing":         "TESTING",
	}
	if !reflect.DeepEqual(vals, expected) {
		t.Fatal("values are not expected ones", conf)
	}

	t.Log(vals)
}

func TestFlagNames(t *testing.T) {
	defer setArgs("/bin/mysoft")()

	var conf Config
	vals, err := new(Loader).GetFlagNames(&conf)

	if err != nil {
		t.Fatal(err)
	}
	expected := map[string]string{
		"LogLevel":        "log-level",
		"Number":          "number",
		"Port":            "port",
		"Stuff.Bar.Thing": "stuff.bar.thing",
		"Stuff.Foo":       "stuff.foo",
		"Testing":         "testing",
	}
	if !reflect.DeepEqual(vals, expected) {
		t.Fatal("values are not expected ones", vals)
	}
	t.Log(vals)
}

func TestDefaults(t *testing.T) {

	var conf Config
	(&Loader{
		NoEnvs:  true,
		NoFlags: true,
	}).Load(&conf)

	ok := conf.Port == 3630 &&
		conf.Number == 42 &&
		conf.LogLevel == "info" &&
		conf.Testing.B == true &&
		conf.Stuff.Foo == 12 &&
		conf.Stuff.Bar.Thing == 666

	if !ok {
		t.Fatal("values are not expected ones")
	}
}

func TestBadDefault(t *testing.T) {
	var conf = struct {
		Port int `default:"forty-two"`
	}{}
	err := Load(&conf)
	if err == nil {
		t.Fatal(err)
	}
	_, err = json.MarshalIndent(conf, "", "  ")
	if err != nil {
		t.Fatal(err)
	}
}

func TestNoDefault(t *testing.T) {
	var conf = struct {
		Port int `help:"port to bind to"`
	}{}
	err := Load(&conf)
	if err == nil {
		t.Fatal(err)
	}
	_, err = json.MarshalIndent(conf, "", "  ")
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkLoadSmall(b *testing.B) {
	defer setArgs("/bin/mysoft", "-log-level", "alert")()
	defer setEnv("PORT", "4242")()
	defer setEnv("TESTING", "true")()
	defer setEnv("STUFF_FOO", "42")()

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var conf Config
		Load(&conf)
	}
}

func BenchmarkLoadLargeDefaults(b *testing.B) {
	defer setArgs("/bin/mysoft")()
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var conf struct {
			S00, S01, S02, S03, S04, S05, S06, S07, S08, S09, S0A, S0B, S0C, S0D, S0E, S0F,
			S10, S11, S12, S13, S14, S15, S16, S17, S18, S19, S1A, S1B, S1C, S1D, S1E, S1F,
			S20, S21, S22, S23, S24, S25, S26, S27, S28, S29, S2A, S2B, S2C, S2D, S2E, S2F,
			S30, S31, S32, S33, S34, S35, S36, S37, S38, S39, S3A, S3B, S3C, S3D, S3E, S3F,
			S40, S41, S42, S43, S44, S45, S46, S47, S48, S49, S4A, S4B, S4C, S4D, S4E, S4F,
			S50, S51, S52, S53, S54, S55, S56, S57, S58, S59, S5A, S5B, S5C, S5D, S5E, S5F,
			S60, S61, S62, S63, S64, S65, S66, S67, S68, S69, S6A, S6B, S6C, S6D, S6E, S6F,
			S70, S71, S72, S73, S74, S75, S76, S77, S78, S79, S7A, S7B, S7C, S7D, S7E, S7F,
			S80, S81, S82, S83, S84, S85, S86, S87, S88, S89, S8A, S8B, S8C, S8D, S8E, S8F,
			S90, S91, S92, S93, S94, S95, S96, S97, S98, S99, S9A, S9B, S9C, S9D, S9E, S9F,
			SA0, SA1, SA2, SA3, SA4, SA5, SA6, SA7, SA8, SA9, SAA, SAB, SAC, SAD, SAE, SAF,
			SB0, SB1, SB2, SB3, SB4, SB5, SB6, SB7, SB8, SB9, SBA, SBB, SBC, SBD, SBE, SBF,
			SC0, SC1, SC2, SC3, SC4, SC5, SC6, SC7, SC8, SC9, SCA, SCB, SCC, SCD, SCE, SCF,
			SD0, SD1, SD2, SD3, SD4, SD5, SD6, SD7, SD8, SD9, SDA, SDB, SDC, SDD, SDE, SDF,
			SE0, SE1, SE2, SE3, SE4, SE5, SE6, SE7, SE8, SE9, SEA, SEB, SEC, SED, SEE, SEF,
			SF0, SF1, SF2, SF3, SF4, SF5, SF6, SF7, SF8, SF9, SFA, SFB, SFC, SFD, SFE, SFF,
			Sfinal uint16 `default:"3630" help:"port to bind to"`
		}
		Load(&conf)
	}
}
