package gonfig

import (
	"flag"
	"fmt"
	"reflect"
	"strconv"
)

func fillReflectValue(v reflect.Value, str string) error {
	// fmt.Println("fill", v.Type())
	switch v.Type().Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		def, err := strconv.ParseUint(str, 0, v.Type().Bits())
		if err != nil {
			return fmt.Errorf("value not a uint: %v", err)
		}
		v.SetUint(def)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		def, err := strconv.ParseInt(str, 0, v.Type().Bits())
		if err != nil {
			return fmt.Errorf("value not an int: %v", err)
		}
		v.SetInt(def)
	case reflect.String:
		v.SetString(str)
	case reflect.Bool:
		def, err := strconv.ParseBool(str)
		if err != nil {
			return fmt.Errorf("value not a bool: %v", err)
		}
		v.SetBool(def)
	default:
		if v.Type().Kind() == reflect.Ptr && v.IsNil() {
			v.Set(reflect.New(v.Type().Elem()))
		}

		if v.Type().Implements(reflect.TypeOf((*flag.Value)(nil)).Elem()) {
			err := v.Interface().(flag.Value).Set(str)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
