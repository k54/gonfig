package gonfig

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

// cleanString drops all characters except [a-zA-Z0-9._-]
func cleanString(in string) string {
	return strings.Map(func(c rune) rune {
		if 'a' <= c && c <= 'z' {
			return c
		}
		if 'A' <= c && c <= 'Z' {
			return c
		}
		if '0' <= c && c <= '9' {
			return c
		}
		switch c {
		case '.', '-', '_':
			return c
		}

		return -1
	}, in)
}

func isNameValid(in string) bool {
	if len(in) < 1 {
		return false
	}
	for i, c := range in {
		if c >= utf8.RuneSelf {
			return false
		}
		if 'a' <= c && c <= 'z' {
			continue
		}
		if 'A' <= c && c <= 'Z' {
			continue
		}
		// first character must be alpha
		if i == 0 {
			return false
		}
		if '0' <= c && c <= '9' {
			continue
		}
		// last character must be alphanum
		if i == len([]rune(in))-1 {
			return false
		}
		if c == '.' || c == '-' || c == '_' {
			continue
		}
		// dont know this char
		return false
	}
	return true
}

func convertCase(source string, transform func(string) string, newDelim rune) string {
	shards := []string{}
	last := 0
	src := []rune(source)
	for i, char := range src {
		if (char == '_' || char == '-' || char == '.' || char == ' ') && last < i {
			shards = append(shards, string(src[last:i]))
			last = i + utf8.RuneLen(char)
			continue
		}
		if (unicode.IsUpper(char) || unicode.IsDigit(char)) && last < i {
			// next rune
			if i == 0 {
				continue
			}
			pr := src[i-1]
			if unicode.IsUpper(pr) || unicode.IsDigit(pr) {
				if i < len(src)-1 {
					if !unicode.IsLower(src[i+1]) {
						continue
					}
				} else {
					continue
				}

			}
			shards = append(shards, string(src[last:i]))
			last = i
		}
	}
	// log.Println(shards, source[last:], last, len([]rune(source))-1)
	if len(src[last:]) > 0 {
		// log.Println("appending last")
		shards = append(shards, transform(string(src[last:])))
	}
	StringsEach(shards, transform)
	return strings.Join(shards, string(newDelim))
}

func StringsEach(in []string, transform func(string) string) {
	for i := 0; i < len(in); i++ {
		in[i] = transform(in[i])
	}
}

func toFlagName(in string) string {
	flagNameShards := strings.Split(in, ".")
	StringsEach(flagNameShards, func(in string) string {
		return convertCase(in, strings.ToLower, '-')
	})
	return strings.Join(flagNameShards, ".")
}

func toEnvName(in string) string {
	envNameShards := strings.Split(in, ".")
	StringsEach(envNameShards, func(in string) string {
		return convertCase(in, strings.ToUpper, '_')
	})
	return strings.Join(envNameShards, "_")
}
